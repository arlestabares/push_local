import 'package:flutter/material.dart';
import 'package:push_local/src/pages/home_page.dart';
import 'package:push_local/src/pages/mensaje_page.dart';
import 'package:push_local/src/push_providers/push_notifications_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

//Con el navigatorKey podre manejar el estado de la navegacion
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();

    final pushProvider = new PushNotificationProvider();
    pushProvider.initNotificaciones();


    //Justo dentro de esta funcion podremos hacer cualquier configuracion
    //de las notificaciones
    pushProvider.mensajes.listen((data) {
      //Navigator.pushNamed(context, 'mensaje');

      print('Data del push  =========');
      print("El valor de la data es = $data");
      
      //Con este navigatorKey podre navegar entre pantallas, en vez de 
      //utilizar Navigator.pushNamed(context, 'mensaje');
      navigatorKey.currentState.pushNamed('mensaje', arguments: data);
      //navigatorKey.currentState.pushNamed('home',arguments: data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      title: 'Push Local',
      initialRoute: 'home',
      routes: {
        'home': (BuildContext context) => HomePage(),
        'mensaje': (BuildContext context) => MensajePages(),
      },
    );
  }
}
