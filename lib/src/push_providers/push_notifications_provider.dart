import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationProvider {
// Iniciar notificaciones.
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  //Nota: Cada vez que creo un StreamController debo crear el metodo dispose().
  final _mensajeStreamController = StreamController<String>.broadcast();
  Stream<String> get mensajes => _mensajeStreamController.stream;

//pedimos permiso para las notificaciones .
  initNotificaciones() {
    _firebaseMessaging.requestNotificationPermissions();

    //Obtener el token del dispositivo, que nos regresa un Future.
    _firebaseMessaging.getToken().then((token) {
      print('======= FCM Token =======');
      print(token);

      //Token generado en consola
      //dZXYZpPVT2qD7Df0n4hvp6:APA91bHaiBHJrajVasM-GekkoUU-I8XaEvDnN48OImz8sRbXyxcSNVxdSwWPeZVbto4f8HfEXN0dDHpqVzzrzVaNtWx743Dopy2uW_wRsjWsd9k5-g0mWtgQTRQX00o5tvezv81AjnKJ
    });
    _firebaseMessaging.configure(
      onMessage: (info) {
        print('======= On Message =======');
        print(info);

        String argumento = 'no-data';
        if (Platform.isAndroid) {
          argumento = info['data']['bike'] ?? 'no-data';
        }
          //Cada que reciba una notificacion, se va a agregar al stream mediante el sink,
          //y el escucha  definido como get mensaje tendra dicha informacion
        _mensajeStreamController.sink.add(argumento);
        return;
      },
      onLaunch: (info) {
        print('======= On Launch =======');
        print(info);

        return;
      },
      onResume: (info) {
        print('======= On Resume =======');
        print(info);

        final notificacion = info ['data']['bike'];
        _mensajeStreamController.sink.add(notificacion);

        // final valorArgumento = info['data']['Bike'];
        // print('===========================');
        // print(valorArgumento);
        
        
        return;
      },
    );
  }

  dispose() {
    _mensajeStreamController?.close();
  }
}
